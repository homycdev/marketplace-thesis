package io.gitlab.homycdev.thesis.domain.products

import cats.Applicative
import cats.data.EitherT
import cats.syntax.all._
import io.gitlab.homycdev.thesis.domain.{PetAlreadyExistsError, PetNotFoundError}

class ProductValidationInterpreter[F[_]: Applicative](repository: ProductRepositoryAlgebra[F])
    extends ProductValidationAlgebra[F] {
  def doesNotExist(pet: ProductItem): EitherT[F, PetAlreadyExistsError, Unit] = EitherT {
    repository.findByNameAndCategory(pet.name, pet.category).map { matches =>
      if (matches.forall(possibleMatch => possibleMatch.bio != pet.bio)) {
        Right(())
      } else {
        Left(PetAlreadyExistsError(pet))
      }
    }
  }

  def exists(petId: Option[Long]): EitherT[F, PetNotFoundError.type, Unit] =
    EitherT {
      petId match {
        case Some(id) =>
          // Ensure is a little tough to follow, it says "make sure this condition is true, otherwise throw the error specified
          // In this example, we make sure that the option returned has a value, otherwise the pet was not found
          repository.get(id).map {
            case Some(_) => Right(())
            case _ => Left(PetNotFoundError)
          }
        case _ =>
          Either.left[PetNotFoundError.type, Unit](PetNotFoundError).pure[F]
      }
    }
}

object ProductValidationInterpreter {
  def apply[F[_]: Applicative](repository: ProductRepositoryAlgebra[F]) =
    new ProductValidationInterpreter[F](repository)
}
