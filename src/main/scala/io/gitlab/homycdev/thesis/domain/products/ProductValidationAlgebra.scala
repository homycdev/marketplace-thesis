package io.gitlab.homycdev.thesis.domain.products

import cats.data.EitherT
import io.gitlab.homycdev.thesis.domain.{PetAlreadyExistsError, PetNotFoundError}

trait ProductValidationAlgebra[F[_]] {
  /* Fails with a PetAlreadyExistsError */
  def doesNotExist(item: ProductItem): EitherT[F, PetAlreadyExistsError, Unit]

  /* Fails with a PetNotFoundError if the item id does not exist or if it is none */
  def exists(petId: Option[Long]): EitherT[F, PetNotFoundError.type, Unit]
}
