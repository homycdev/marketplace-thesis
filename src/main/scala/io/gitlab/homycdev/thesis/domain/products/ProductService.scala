package io.gitlab.homycdev.thesis.domain.products

import cats.Functor
import cats.data._
import cats.Monad
import cats.syntax.all._
import io.gitlab.homycdev.thesis.domain.{PetAlreadyExistsError, PetNotFoundError}

/** The entry point to our domain, works with repositories and validations to implement behavior
  * @param repository where we get our data
  * @param validation something that provides validations to the service
  * @tparam F - this is the container for the things we work with, could be scala.concurrent.Future, Option, anything
  *           as long as it is a Monad
  */
class ProductService[F[_]](
    repository: ProductRepositoryAlgebra[F],
    validation: ProductValidationAlgebra[F],
) {
  def create(
      item: ProductItem,
  )(implicit M: Monad[F]): EitherT[F, PetAlreadyExistsError, ProductItem] =
    for {
      _ <- validation.doesNotExist(item)
      saved <- EitherT.liftF(repository.create(item))
    } yield saved

  /* Could argue that we could make this idempotent on put and not check if the item exists */
  def update(
      item: ProductItem,
  )(implicit M: Monad[F]): EitherT[F, PetNotFoundError.type, ProductItem] =
    for {
      _ <- validation.exists(item.id)
      saved <- EitherT.fromOptionF(repository.update(item), PetNotFoundError)
    } yield saved

  def get(id: Long)(implicit F: Functor[F]): EitherT[F, PetNotFoundError.type, ProductItem] =
    EitherT.fromOptionF(repository.get(id), PetNotFoundError)

  /* In some circumstances we may care if we actually delete the pet; here we are idempotent and do not care */
  def delete(id: Long)(implicit F: Functor[F]): F[Unit] =
    repository.delete(id).as(())

  def list(pageSize: Int, offset: Int): F[List[ProductItem]] =
    repository.list(pageSize, offset)

  def findByStatus(statuses: NonEmptyList[ProductStatus]): F[List[ProductItem]] =
    repository.findByStatus(statuses)

  def findByTag(tags: NonEmptyList[String]): F[List[ProductItem]] =
    repository.findByTag(tags)
}

object ProductService {
  def apply[F[_]](
      repository: ProductRepositoryAlgebra[F],
      validation: ProductValidationAlgebra[F],
  ): ProductService[F] =
    new ProductService[F](repository, validation)
}
