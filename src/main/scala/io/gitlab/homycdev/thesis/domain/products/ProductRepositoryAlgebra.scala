package io.gitlab.homycdev.thesis.domain.products

import cats.data.NonEmptyList

trait ProductRepositoryAlgebra[F[_]] {
  def create(item: ProductItem): F[ProductItem]

  def update(pet: ProductItem): F[Option[ProductItem]]

  def get(id: Long): F[Option[ProductItem]]

  def delete(id: Long): F[Option[ProductItem]]

  def findByNameAndCategory(name: String, category: String): F[Set[ProductItem]]

  def list(pageSize: Int, offset: Int): F[List[ProductItem]]

  def findByStatus(status: NonEmptyList[ProductStatus]): F[List[ProductItem]]

  def findByTag(tags: NonEmptyList[String]): F[List[ProductItem]]
}
