package io.gitlab.homycdev.thesis.domain

import products.ProductItem
import users.User

sealed trait ValidationError extends Product with Serializable
case class PetAlreadyExistsError(item: ProductItem) extends ValidationError
case object PetNotFoundError extends ValidationError
case object OrderNotFoundError extends ValidationError
case object UserNotFoundError extends ValidationError
case class UserAlreadyExistsError(user: User) extends ValidationError
case class UserAuthenticationFailedError(userName: String) extends ValidationError
