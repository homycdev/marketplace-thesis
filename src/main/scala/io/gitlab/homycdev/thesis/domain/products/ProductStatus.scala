package io.gitlab.homycdev.thesis.domain.products

import enumeratum._

import scala.collection.immutable

sealed trait ProductStatus extends EnumEntry

case object ProductStatus extends Enum[ProductStatus] with CirceEnum[ProductStatus] {
  case object Available extends ProductStatus
  case object Pending extends ProductStatus
  case object Unavailable extends ProductStatus

  val values: immutable.IndexedSeq[ProductStatus] = findValues
}
