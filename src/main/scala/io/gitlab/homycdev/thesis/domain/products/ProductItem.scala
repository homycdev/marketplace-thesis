package io.gitlab.homycdev.thesis.domain.products

case class ProductItem(
    name: String,
    category: String,
    bio: String,
    status: ProductStatus = ProductStatus.Available,
    tags: Set[String] = Set.empty,
    photoUrls: Set[String] = Set.empty,
    id: Option[Long] = None,
)
