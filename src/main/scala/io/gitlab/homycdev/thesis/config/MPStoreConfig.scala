package io.gitlab.homycdev.thesis.config

final case class ServerConfig(host: String, port: Int)
final case class MPStoreConfig(db: DatabaseConfig, server: ServerConfig)
