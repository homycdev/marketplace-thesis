package io.gitlab.homycdev.thesis.infrastructure.repository.doobie

import cats.data._
import cats.syntax.all._
import doobie._
import doobie.implicits._
import SQLPagination._
import cats.effect.Bracket
import io.gitlab.homycdev.thesis.domain.products.{ProductItem, ProductRepositoryAlgebra, ProductStatus}

private object ItemSQL {
  /* We require type StatusMeta to handle our ADT Status */
  implicit val StatusMeta: Meta[ProductStatus] =
    Meta[String].imap(ProductStatus.withName)(_.entryName)

  /* This is used to marshal our sets of strings */
  implicit val SetStringMeta: Meta[Set[String]] =
    Meta[String].imap(_.split(',').toSet)(_.mkString(","))

  def insert(pet: ProductItem): Update0 = sql"""
    INSERT INTO ITEM (NAME, CATEGORY, BIO, STATUS, TAGS, PHOTO_URLS)
    VALUES (${pet.name}, ${pet.category}, ${pet.bio}, ${pet.status}, ${pet.tags}, ${pet.photoUrls})
  """.update

  def update(pet: ProductItem, id: Long): Update0 = sql"""
    UPDATE ITEM
    SET NAME = ${pet.name}, BIO = ${pet.bio}, STATUS = ${pet.status}, TAGS = ${pet.tags}, PHOTO_URLS = ${pet.photoUrls}
    WHERE id = $id
  """.update

  def select(id: Long): Query0[ProductItem] = sql"""
    SELECT NAME, CATEGORY, BIO, STATUS, TAGS, PHOTO_URLS, ID
    FROM ITEM
    WHERE ID = $id
  """.query

  def delete(id: Long): Update0 = sql"""
    DELETE FROM ITEM WHERE ID = $id
  """.update

  def selectByNameAndCategory(name: String, category: String): Query0[ProductItem] = sql"""
    SELECT NAME, CATEGORY, BIO, STATUS, TAGS, PHOTO_URLS, ID
    FROM ITEM
    WHERE NAME = $name AND CATEGORY = $category
  """.query[ProductItem]

  def selectAll: Query0[ProductItem] = sql"""
    SELECT NAME, CATEGORY, BIO, STATUS, TAGS, PHOTO_URLS, ID
    FROM ITEM
    ORDER BY NAME
  """.query

  def selectByStatus(statuses: NonEmptyList[ProductStatus]): Query0[ProductItem] =
    (
      sql"""
      SELECT NAME, CATEGORY, BIO, STATUS, TAGS, PHOTO_URLS, ID
      FROM ITEM
      WHERE """ ++ Fragments.in(fr"STATUS", statuses)
    ).query

  def selectTagLikeString(tags: NonEmptyList[String]): Query0[ProductItem] = {
    /* Handle dynamic construction of query based on multiple parameters */

    /* To piggyback off of comment of above reference about tags implementation, findByTag uses LIKE for partial matching
    since tags is (currently) implemented as a comma-delimited string */
    val tagLikeString: String = tags.toList.mkString("TAGS LIKE '%", "%' OR TAGS LIKE '%", "%'")
    (sql"""SELECT NAME, CATEGORY, BIO, STATUS, TAGS, PHOTO_URLS, ID
         FROM ITEM
         WHERE """ ++ Fragment.const(tagLikeString))
      .query[ProductItem]
  }
}

class DoobieProductRepositoryInterpreter[F[_]: Bracket[*[_], Throwable]](val xa: Transactor[F])
    extends ProductRepositoryAlgebra[F] {
  import ItemSQL._

  def create(pet: ProductItem): F[ProductItem] =
    insert(pet).withUniqueGeneratedKeys[Long]("ID").map(id => pet.copy(id = id.some)).transact(xa)

  def update(pet: ProductItem): F[Option[ProductItem]] =
    OptionT
      .fromOption[ConnectionIO](pet.id)
      .semiflatMap(id => ItemSQL.update(pet, id).run.as(pet))
      .value
      .transact(xa)

  def get(id: Long): F[Option[ProductItem]] = select(id).option.transact(xa)

  def delete(id: Long): F[Option[ProductItem]] =
    OptionT(select(id).option).semiflatMap(pet => ItemSQL.delete(id).run.as(pet)).value.transact(xa)

  def findByNameAndCategory(name: String, category: String): F[Set[ProductItem]] =
    selectByNameAndCategory(name, category).to[List].transact(xa).map(_.toSet)

  def list(pageSize: Int, offset: Int): F[List[ProductItem]] =
    paginate(pageSize, offset)(selectAll).to[List].transact(xa)

  def findByStatus(statuses: NonEmptyList[ProductStatus]): F[List[ProductItem]] =
    selectByStatus(statuses).to[List].transact(xa)

  def findByTag(tags: NonEmptyList[String]): F[List[ProductItem]] =
    selectTagLikeString(tags).to[List].transact(xa)
}

object DoobieProductRepositoryInterpreter {
  def apply[F[_]: Bracket[*[_], Throwable]](
      xa: Transactor[F],
  ): DoobieProductRepositoryInterpreter[F] =
    new DoobieProductRepositoryInterpreter(xa)
}
