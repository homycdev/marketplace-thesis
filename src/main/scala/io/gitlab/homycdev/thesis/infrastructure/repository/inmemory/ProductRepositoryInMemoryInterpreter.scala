package io.gitlab.homycdev.thesis.infrastructure.repository.inmemory

import scala.collection.concurrent.TrieMap
import scala.util.Random
import cats._
import cats.data.NonEmptyList
import cats.implicits._
import io.gitlab.homycdev.thesis.domain.products.{ProductItem, ProductRepositoryAlgebra, ProductStatus}

class ProductRepositoryInMemoryInterpreter[F[_]: Applicative] extends ProductRepositoryAlgebra[F] {
  private val cache = new TrieMap[Long, ProductItem]

  private val random = new Random

  def create(pet: ProductItem): F[ProductItem] = {
    val id = random.nextLong()
    val toSave = pet.copy(id = id.some)
    cache += (id -> pet.copy(id = id.some))
    toSave.pure[F]
  }

  def update(pet: ProductItem): F[Option[ProductItem]] = pet.id.traverse { id =>
    cache.update(id, pet)
    pet.pure[F]
  }

  def get(id: Long): F[Option[ProductItem]] = cache.get(id).pure[F]

  def delete(id: Long): F[Option[ProductItem]] = cache.remove(id).pure[F]

  def findByNameAndCategory(name: String, category: String): F[Set[ProductItem]] =
    cache.values
      .filter(p => p.name == name && p.category == category)
      .toSet
      .pure[F]

  def list(pageSize: Int, offset: Int): F[List[ProductItem]] =
    cache.values.toList.sortBy(_.name).slice(offset, offset + pageSize).pure[F]

  def findByStatus(statuses: NonEmptyList[ProductStatus]): F[List[ProductItem]] =
    cache.values.filter(p => statuses.exists(_ == p.status)).toList.pure[F]

  def findByTag(tags: NonEmptyList[String]): F[List[ProductItem]] = {
    val tagSet = tags.toNes
    cache.values.filter(_.tags.exists(tagSet.contains(_))).toList.pure[F]
  }
}

object ProductRepositoryInMemoryInterpreter {
  def apply[F[_]: Applicative]() = new ProductRepositoryInMemoryInterpreter[F]()
}
