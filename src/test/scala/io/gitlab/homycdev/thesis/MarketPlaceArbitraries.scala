package io.gitlab.homycdev.thesis

import cats.effect.IO
import io.gitlab.homycdev.thesis.domain.authentication.SignupRequest
import io.gitlab.homycdev.thesis.domain.orders.OrderStatus._
import io.gitlab.homycdev.thesis.domain.orders._
import io.gitlab.homycdev.thesis.domain.products.ProductStatus._
import io.gitlab.homycdev.thesis.domain.products._
import io.gitlab.homycdev.thesis.domain.users.{Role, _}
import io.gitlab.homycdev.thesis.domain.{orders, products}
import org.scalacheck.Arbitrary.arbitrary
import org.scalacheck._
import tsec.authentication.AugmentedJWT
import tsec.common.SecureRandomId
import tsec.jws.mac._
import tsec.jwt.JWTClaims
import tsec.mac.jca._

import java.time.Instant

trait MarketPlaceArbitraries {
  val userNameLength = 16
  val userNameGen: Gen[String] = Gen.listOfN(userNameLength, Gen.alphaChar).map(_.mkString)

  implicit val instant: Arbitrary[Instant] = Arbitrary[Instant] {
    for {
      millis <- Gen.posNum[Long]
    } yield Instant.ofEpochMilli(millis)
  }

  implicit val orderStatus: Arbitrary[OrderStatus] = Arbitrary[OrderStatus] {
    Gen.oneOf(Approved, Delivered, Placed)
  }

  def order(userId: Option[Long]): Arbitrary[Order] = Arbitrary[Order] {
    for {
      petId <- Gen.posNum[Long]
      shipDate <- Gen.option(instant.arbitrary)
      status <- arbitrary[OrderStatus]
      complete <- arbitrary[Boolean]
      id <- Gen.option(Gen.posNum[Long])
    } yield orders.Order(petId, shipDate, status, complete, id, userId)
  }

  implicit val orderNoUser: Arbitrary[Order] = order(None)

  implicit val petStatus: Arbitrary[ProductStatus] = Arbitrary[ProductStatus] {
    Gen.oneOf(Available, Pending, Unavailable)
  }

  implicit val item: Arbitrary[ProductItem] = Arbitrary[ProductItem] {
    for {
      name <- Gen.nonEmptyListOf(Gen.asciiPrintableChar).map(_.mkString)
      category <- arbitrary[String]
      bio <- arbitrary[String]
      status <- arbitrary[ProductStatus]
      numTags <- Gen.choose(0, 10)
      tags <- Gen.listOfN(numTags, Gen.alphaStr).map(_.toSet)
      photoUrls <- Gen
        .listOfN(numTags, Gen.alphaStr)
        .map(_.map(x => s"http://$x.com"))
        .map(_.toSet)
      id <- Gen.option(Gen.posNum[Long])
    } yield products.ProductItem(name, category, bio, status, tags, photoUrls, id)
  }

  implicit val role: Arbitrary[Role] = Arbitrary[Role](Gen.oneOf(Role.values.toIndexedSeq))

  implicit val user: Arbitrary[User] = Arbitrary[User] {
    for {
      userName <- userNameGen
      firstName <- arbitrary[String]
      lastName <- arbitrary[String]
      email <- arbitrary[String]
      password <- arbitrary[String]
      phone <- arbitrary[String]
      id <- Gen.option(Gen.posNum[Long])
      role <- arbitrary[Role]
    } yield User(userName, firstName, lastName, email, password, phone, id, role)
  }

  case class AdminUser(value: User)
  case class CustomerUser(value: User)

  implicit val adminUser: Arbitrary[AdminUser] = Arbitrary {
    user.arbitrary.map(user => AdminUser(user.copy(role = Role.Admin)))
  }

  implicit val customerUser: Arbitrary[CustomerUser] = Arbitrary {
    user.arbitrary.map(user => CustomerUser(user.copy(role = Role.Customer)))
  }

  implicit val userSignup: Arbitrary[SignupRequest] = Arbitrary[SignupRequest] {
    for {
      userName <- userNameGen
      firstName <- arbitrary[String]
      lastName <- arbitrary[String]
      email <- arbitrary[String]
      password <- arbitrary[String]
      phone <- arbitrary[String]
      role <- arbitrary[Role]
    } yield SignupRequest(userName, firstName, lastName, email, password, phone, role)
  }

  implicit val secureRandomId: Arbitrary[SecureRandomId] = Arbitrary[SecureRandomId] {
    arbitrary[String].map(SecureRandomId.apply)
  }

  implicit val jwtMac: Arbitrary[JWTMac[HMACSHA256]] = Arbitrary {
    for {
      key <- Gen.const(HMACSHA256.unsafeGenerateKey)
      claims <- Gen.finiteDuration.map(exp =>
        JWTClaims.withDuration[IO](expiration = Some(exp)).unsafeRunSync(),
      )
    } yield JWTMacImpure
      .build[HMACSHA256](claims, key)
      .getOrElse(throw new Exception("Inconceivable"))
  }

  implicit def augmentedJWT[A, I](implicit
      arb1: Arbitrary[JWTMac[A]],
      arb2: Arbitrary[I],
  ): Arbitrary[AugmentedJWT[A, I]] =
    Arbitrary {
      for {
        id <- arbitrary[SecureRandomId]
        jwt <- arb1.arbitrary
        identity <- arb2.arbitrary
        expiry <- arbitrary[Instant]
        lastTouched <- Gen.option(arbitrary[Instant])
      } yield AugmentedJWT(id, jwt, identity, expiry, lastTouched)
    }
}

object MarketPlaceArbitraries extends MarketPlaceArbitraries
