package io.gitlab.homycdev.thesis
package infrastructure.endpoint

import cats.data.NonEmptyList
import cats.effect._
import io.circe.generic.auto._
import io.gitlab.homycdev.thesis.domain.products._
import io.gitlab.homycdev.thesis.domain.users._
import io.gitlab.homycdev.thesis.infrastructure.repository.inmemory._
import org.http4s._
import org.http4s.circe._
import org.http4s.client.dsl.Http4sClientDsl
import org.http4s.dsl._
import org.http4s.implicits._
import org.http4s.server.Router
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks
import tsec.mac.jca.HMACSHA256

class ProductItemEndpointsSpec
    extends AnyFunSuite
    with Matchers
    with ScalaCheckPropertyChecks
    with MarketPlaceArbitraries
    with Http4sDsl[IO]
    with Http4sClientDsl[IO] {
  implicit val petEnc: EntityEncoder[IO, ProductItem] = jsonEncoderOf
  implicit val petDec: EntityDecoder[IO, ProductItem] = jsonOf

  def getTestResources(): (AuthTest[IO], HttpApp[IO], ProductRepositoryInMemoryInterpreter[IO]) = {
    val userRepo = UserRepositoryInMemoryInterpreter[IO]()
    val petRepo = ProductRepositoryInMemoryInterpreter[IO]()
    val petValidation = ProductValidationInterpreter[IO](petRepo)
    val productService = ProductService[IO](petRepo, petValidation)
    val auth = new AuthTest[IO](userRepo)
    val petEndpoint =
      ProductEndpoints.endpoints[IO, HMACSHA256](productService, auth.securedRqHandler)
    val petRoutes = Router(("/products", petEndpoint)).orNotFound
    (auth, petRoutes, petRepo)
  }

  test("create item") {
    val (auth, petRoutes, _) = getTestResources()

    forAll { item: ProductItem =>
      (for {
        request <- POST(item, uri"/products")
        response <- petRoutes.run(request)
      } yield response.status shouldEqual Unauthorized).unsafeRunSync()
    }

    forAll { (item: ProductItem, user: User) =>
      (for {
        request <- POST(item, uri"/products")
          .flatMap(auth.embedToken(user, _))
        response <- petRoutes.run(request)
      } yield response.status shouldEqual Ok).unsafeRunSync()
    }

    forAll { (item: ProductItem, user: User) =>
      (for {
        createRq <- POST(item, uri"/products")
          .flatMap(auth.embedToken(user, _))
        response <- petRoutes.run(createRq)
        createdPet <- response.as[ProductItem]
        getRq <- GET(Uri.unsafeFromString(s"/products/${createdPet.id.get}"))
          .flatMap(auth.embedToken(user, _))
        response2 <- petRoutes.run(getRq)
      } yield {
        response.status shouldEqual Ok
        response2.status shouldEqual Ok
      }).unsafeRunSync()
    }
  }

  test("update item") {
    val (auth, petRoutes, _) = getTestResources()

    forAll { (item: ProductItem, user: AdminUser) =>
      (for {
        createRequest <- POST(item, uri"/products")
          .flatMap(auth.embedToken(user.value, _))
        createResponse <- petRoutes.run(createRequest)
        createdPet <- createResponse.as[ProductItem]
        petToUpdate = createdPet.copy(name = createdPet.name.reverse)
        updateRequest <- PUT(petToUpdate, Uri.unsafeFromString(s"/products/${petToUpdate.id.get}"))
          .flatMap(auth.embedToken(user.value, _))
        updateResponse <- petRoutes.run(updateRequest)
        updatedPet <- updateResponse.as[ProductItem]
      } yield updatedPet.name shouldEqual item.name.reverse).unsafeRunSync()
    }
  }

  test("find by tag") {
    val (auth, petRoutes, petRepo) = getTestResources()

    forAll { (item: ProductItem, user: AdminUser) =>
      (for {
        createRequest <- POST(item, uri"/products")
          .flatMap(auth.embedToken(user.value, _))
        createResponse <- petRoutes.run(createRequest)
        createdPet <- createResponse.as[ProductItem]
      } yield createdPet.tags.toList.headOption match {
        case Some(tag) =>
          val petsFoundByTag = petRepo.findByTag(NonEmptyList.of(tag)).unsafeRunSync()
          petsFoundByTag.contains(createdPet) shouldEqual true
        case _ => ()
      }).unsafeRunSync()
    }
  }
}
